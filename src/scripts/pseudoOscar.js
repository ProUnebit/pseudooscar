import { pseudoOscar } from './modules/Game'

function DOMIsReady() { if (document.getElementById('pseudo-oscar')) pseudoOscar.gameInit() }

document.addEventListener('DOMContentLoaded', DOMIsReady);
