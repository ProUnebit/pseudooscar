import axios from 'axios'

const instance = axios.create()

class ServerDataDialogue {
    constructor(baseURL) {
        this.baseURL = baseURL
    }

    async getData() {
        try {
            let res = await axios.get(this.baseURL)
            let data = res.data.data
            return data;

        } catch(err) {
            console.log('Sorry, failed try get data. Error: ', err)
            if (err.res) {
              console.log(err.res.data)
              console.log(err.res.status)
              console.log(err.res.headers)
            }
        }
    }

    async postData() {
        try {
            let res = await axios.post(this.baseURL,
                {data: {

                }}
        )

        } catch(err) {
            console.log('Sorry, failed try post data. Error: ', err)
        }
    }

    async putData() {
        try {
            let res = await axios.put(this.baseURL, {

            })

        } catch(err) {
            console.log('Sorry, failed try put data. Error: ', err)
        }
    }
}

export const DataSup = new ServerDataDialogue(
    'https://crossx.m24.ru/oskar/web/site/get-api'
)
