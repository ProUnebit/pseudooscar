import _ from 'lodash-core'
import voca from 'voca'
import Chart from 'chart.js'
import 'lightgallery.js'
import '../../common_libraries/lightgallery/lg-zoom.min.js'
import '../../common_libraries/lightgallery/css/lightgallery.min.css'
import '../../common_libraries/lightgallery/css/lg-transitions.min.css'
import 'css-ripple-effect'
// Custom modules
import { DataSup } from './ServerDataDialogue/ServerDataDialogue'
import AnimationGSAP from './AnimationGSAP/AnimationGSAP'
import SocNetShare from './SocNetShare/SocNetShare'
// Helpers
import {
    helperRandomInteger,
    helperFindIndex,
    helperShuffleArr,
    helperChunkArray
} from '../partials/helpers'

let boo = true;

class Game {
    constructor(id) {
        // DMZ-object
        this.ø = Object.create( null )
        // ID
        this.id = id
        // Data
        this.gameDataArr
        // Sections
        this.gameMotherContainer
        this.gameIntroBox
        this.gameLoaderBox
        this.gameVoteBox
        this.gameResultsBox
        // Values
        this.gameStep
        this.gameInTotalSteps
        this.gamePassedVoteArr
    }

    async gameInit() {

        this.gameDataArr = await DataSup.getData().catch(err => console.error(err))
        this.gameInTotalSteps = this.gameDataArr.length
        this.gameStep = 0

        this.gameMotherContainer = document.querySelector(`.${this.id}__content`)
        this.gameIntroBox = this.gameMotherContainer.querySelector(`.${this.id}__intro-box`)
        this.gameLoaderBox = this.gameMotherContainer.querySelector(`.${this.id}__loader-box`)
        this.gameVoteBox = this.gameMotherContainer.querySelector(`.${this.id}__vote-box`)
        this.gameResultsBox = this.gameMotherContainer.querySelector(`.${this.id}__results-box`)

        document.getElementById('video-gallery').addEventListener('click', function() {
        lightGallery(document.getElementById('video-gallery'), {
            mode: 'lg-slide-circular',
            cssEasing : 'cubic-bezier(0.25, 0, 0.25, 1)',
            nextHtml: '<div style="height: 100px; width: 100px; background-color: red;"></div>',
            download: false,
            counter: false,
            zoom: true,
            dynamic: true,
            dynamicEl: [{
                "src": 'https://raw.githubusercontent.com/ProUnebit/Reminder_ToMe_Reminder/master/src/assets/img/7svh.jpg',
                'thumb': 'https://raw.githubusercontent.com/ProUnebit/Reminder_ToMe_Reminder/master/src/assets/img/7svh.jpg',
                'subHtml': '<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>'
            }, {
                'src': 'https://raw.githubusercontent.com/ProUnebit/Reminder_ToMe_Reminder/master/src/assets/img/3codd.jpg',
                'thumb': 'https://raw.githubusercontent.com/ProUnebit/Reminder_ToMe_Reminder/master/src/assets/img/3codd.jpg',
                'subHtml': "<h4>Bowness Bay</h4><p>A beautiful Sunrise this morning taken En-route to Keswick not one as planned but I'm extremely happy I was passing the right place at the right time....</p>"
            }, {
                'src': 'https://raw.githubusercontent.com/ProUnebit/Reminder_ToMe_Reminder/master/src/assets/img/1mkad.jpg',
                'thumb': 'https://raw.githubusercontent.com/ProUnebit/Reminder_ToMe_Reminder/master/src/assets/img/1mkad.jpg',
                'subHtml': "<h4>Coniston Calmness</h4><p>Beautiful morning</p>"
            }]
        })
    })

    console.log(this.id + ' init')

    this.gameIntroBoxDisplaying()
    this.gameIntroPhaseActive()
    }

    gameIntroBoxDisplaying() {
        this.gameIntroBox.classList.toggle('game-actions__displaying-on--flex')
    }
    gameLoaderBoxDisplaying() {
        this.gameLoaderBox.classList.toggle('game-actions__displaying-on--flex')
    }
    gameVoteBoxDisplaying() {
        this.gameVoteBox.classList.toggle('game-actions__displaying-on--flex')
    }
    gameResultsBoxDisplaying() {
        this.gameResultsBox.classList.toggle('game-actions__displaying-on--flex')
    }

    gameIntroPhaseActive() {
        console.log('IntroPhaseActive')
    }
    gameVotePhaseActive() {

    }
    gameResultsPhaseActive() {

    }

    async a(args) {

        const { a, b } = args

        try {

            let c = this.retPromise()
            let rollerData = await fetch('https://raw.githubusercontent.com/ProUnebit/Reminder_ToMe_Reminder/master/roller.json')
                .then(res => res.json())
                .then(data => data[0])
            console.log(rollerData)
            console.log('lala', a, b, await c)
            setTimeout(() => console.log('done!'), 500)

        } catch(err) {

            console.log('Sorry, failed try. Error: ', err)
        }

    }

    retPromise() {
        return new Promise((res, rej) => {
            setTimeout(() => {
                if (boo) {
                    res('yup')
                } else {
                    rej('nope')
                }
            }, 4000)
        })
    }
}

export const pseudoOscar = new Game(
    'pseudo-oscar'
)
